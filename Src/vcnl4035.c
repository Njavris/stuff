#include "vcnl4035.h"

enum gest_states {
	BOOT_STATE = 0,
	NORMALIZATION_STATE,
	WAIT_STATE,
	GESTURE_STATE,
	DONE_STATE,
};

static unsigned short data_points[DATA_POINT_CNT][3];

static void clear_int(struct vcnl4035_dev *dev) {
	i2c_read(dev->i2c_addr, 0xd);
}

int vcnl4035_init(struct vcnl4035_dev *dev, uint8_t i2c_addr, 
		uint16_t (*i2c_read)(uint16_t dev_id, uint8_t addr),
		void (*i2c_write)(uint8_t dev_id, uint16_t addr, uint16_t data)) {
	dev->i2c_addr = i2c_addr;
	dev->i2c_read = i2c_read;
	dev->i2c_write = i2c_write;
	return 0;
}


void get_normal(unsigned short *data) {
	unsigned tmp[3] = {0, 0, 0};

	for (int i = 0; i < DATA_POINT_CNT; i++) {
		for (int j = 0; j < 3; j++)
			tmp[j] += data_points[i][j];
	}

	for (int i = 0; i < 3; i++)
	data[i] = tmp[i] / DATA_POINT_CNT;
}

void decode_gesture(unsigned cnt) {
	printf("\n\r");
	for (int i = 0; i < cnt; i++) {
		for (int j = 0; j < 3; j++)
		printf("%04x ", data_points[i][j]);
		printf("\n\r");
	}
	printf("Unknown\n\r");
}

static void i2c_reg_set(struct vcnl4035_dev *dev, uint16_t reg_addr, uint16_t set, uint16_t clr) {
	uint16_t reg_val = dev->i2c_read(dev, reg_addr);
	reg_val &= ~clr;
	reg_val |= set;
	dev->i2c_write(dev->i2c_addr, reg_addr, reg_val);
}

static void ps_trig(struct vcnl4035_dev *dev) {
	i2c_reg_set(dev, 0x4, BIT(2), 0);
}

void dump_i2c_regs(struct vcnl4035_dev *dev) {
	int i;
	printf("\r\nRegister contents");
	for (i = 0; i < 0x10; i++) {
		if (!(i % 8))
			printf("\r\n%02x: ", i);
		printf("%04x ", dev->i2c_read(dev->i2c_addr, i));
	}
	printf("\r\n");
}

void gest_task(struct vcnl4035_dev *dev) {
	static unsigned gest_state = BOOT_STATE;
	static unsigned cnt;
//	static unsigned short normal_data[3];
	unsigned short data[3];

	if (gest_state != BOOT_STATE && !data_avail)
		return;

	if (data_avail) {
		data_avail = 0;
		clear_int(dev);
		for (int i = 0; i < 3; i++)
			data[i] = dev->i2c_read(dev->i2c_addr, 0x8 + i);
	}

	switch (gest_state) {
		case BOOT_STATE:
			printf("BOOT_STATE\n\r");
			memset(data_points, 0, sizeof(data_points));
			gest_state = NORMALIZATION_STATE;
			cnt = 0;
			printf("NORMALIZATION_STATE\n\r");
			dev->i2c_write(dev->i2c_addr, 0x3, 0xc000);
			dev->i2c_write(dev->i2c_addr, 0x4, 0x0209);
			dump_i2c_regs(dev);
			ps_trig(dev);
			return;
		case NORMALIZATION_STATE:
//			for (int i = 0; i < 3; i++) {
//				printf("%04x ", data[i]);
//				data_points[cnt][i] = data[i];
//			}
//			printf("\r\n");
//			cnt++;
//			if (cnt == 0x80) {
//				get_normal(normal_data);
//				for (int i = 0; i < 3; i++)
//					printf("####%04x ",normal_data[i]);
//				printf("\n\r");
			gest_state = WAIT_STATE;
			printf("WAIT_STATE\n\r");
			cnt = 0;
//}
			break;
		case WAIT_STATE:
			for (int i = 0; i < 3; i++)
				printf("%04x ", data[i]);
			if ((data[0] > GEST_THD) || (data[1] > GEST_THD) || (data[2] > GEST_THD)) {
				gest_state = GESTURE_STATE;
				printf("GESTURE_STATE\n\r");
				cnt = 0;
				memset(data_points, 0, sizeof(data_points));
			}
			printf("\r");
			break;
		case GESTURE_STATE:
			for (int i = 0; i < 3; i++) {
				data_points[cnt][i] = data[i];
			}

			if (((data[0] < GEST_THD) && (data[1] < GEST_THD) && (data[2] < GEST_THD)) ||
					(cnt == DATA_POINT_CNT)) {
				decode_gesture(cnt);
				printf("DONE! cnt: %d\n\r", cnt);
//				gest_state = WAIT_STATE;
//				printf("WAIT_STATE\n\r");
				gest_state = DONE_STATE;
				cnt = 0;
			}
			cnt++;
			break;
		case DONE_STATE:
			break;
	}

	ps_trig(dev);
}


