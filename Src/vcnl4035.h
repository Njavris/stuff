#ifndef __VCNL4035_H__
#define __VCNL4035_H__

#define DATA_POINT_CNT	0x80
#define GEST_THD	0x100

enum gest_res {
	GEST_NONE = 0,
	GEST_UP,
	GEST_DOWN,
	GEST_LEFT,
	GEST_RIGHT,
	GEST_ERROR,
	GEST_MAX,
};

struct vcnl4035_dev {
	uint8_t i2c_addr;
	uint16_t  (*i2c_read)(uint16_t dev_id, uint8_t addr);
	void (*i2c_write)(uint8_t dev_id, uint16_t addr, uint16_t data);
	uint8_t data_avail;
	enum gest_res;
};

inline void vcnl5035_irq_cb(struct vcnl4035_dev *dev) {
	dev->data_avail = 1;
}

int vcnl4035_init(struct vcnl4035_dev *dev, uint8_t i2c_addr, 
	uint16_t (*i2c_read)(uint16_t dev_id, uint8_t addr),
	void (*i2c_write)(uint8_t dev_id, uint16_t addr, uint16_t data));

void gest_task(struct vcnl4035_dev *dev);

#endif
